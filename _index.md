+++
title = "拔剑四顾心茫然"
description = ""
+++

<button class="btn h1" type="button" data-toggle="collapse" data-target="#collapse_1797232c6c" style="outline:none;color:#c05b4d;" aria-expanded="true">
	<i class="fas fa-indent"></i>
</button>

<div class="mb-5 show" id="collapse_1797232c6c" style="">
	<div class="alert alert-info" role="alert">
		Salvation lies within.
	</div>
</div>

<button class="btn h1" type="button" data-toggle="collapse" data-target="#collapse_1797232c8c" style="outline:none;color:#c05b4d;" aria-expanded="true">
	<i class="fas fa-directions"></i>
</button>
<div class="row mb-5 show" id="collapse_1797232c8c">
	<div class="col-md-4" style="margin-bottom:8px;color:#34495e;">
		<div class="card flex-row border-0 bg-light">
			<div class="mt-3" style="margin-left:5px;">
				<span class="fas fa-pencil-alt fa-2x text-dark"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					md
				</h5>
				<p class="card-text text-muted">
					<a href="/note/" style="color:green"><i class="fab fa-markdown"></i> 文档</a> 
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4" style="margin-bottom:8px;color:#34495e;">
		<div class="card flex-row border-0 bg-light">
			<div class="mt-3" style="margin-left:5px;">
				<span class="fa fa-terminal fa-2x"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					ssh
				</h5>
				<p class="card-text text-muted">
					<a href="/@]Y_ZMW" style="color:green">webssh://<i class="fab fa-raspberry-pi" style="color:green"></i></a>
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-4" style="margin-bottom:8px;color:#34495e;">
		<div class="card flex-row border-0 bg-light">
			<div class="mt-3" style="margin-left:5px;">
				<span class="fas fa-ellipsis-h fa-2x"></span>
			</div>
			<div class="card-body pl-2">
				<h5 class="card-title">
					...
				</h5>
				<p class="card-text text-muted">
					<a href="#" style="color:green"><i class="fas fa-ellipsis-h"></i></a>
				</p>
			</div>
		</div>
	</div>
</div>

<button class="btn h1" type="button" data-toggle="collapse" data-target="#collapse_1797232c7c" style="outline:none;color:#c05b4d;display:none;" aria-expanded="true">
	<i class="fas fa-code"></i>
</button>

<div class="collapse" id="collapse_1797232c7c" style="">

```python
aa=[51, 46, 49, 52, 49, 53, 57, 50]
bb=[64, 93, 89, 27, 89, 90, 74, 70]

ret = []
for i in range(8):
	ret.append(chr(aa[i] ^ bb[i]))

ssh = ''.join(ret)
```
</div>

