# README #

> [个人博客](https://why.natapp4.cc)，托管于 [树莓派4b](https://www.raspberrypi.org/)，网页使用 [hugo](https://gohugo.io/) 生成。



### 使用的主题 ###

- [Ace-documentation](https://themes.gohugo.io/ace-documentation/) 		主页样式
- [roots](https://themes.gohugo.io/the-roots-home/)                                   博客文章样式



### 使用的第三方库及网站服务

- [MathJax](https://www.mathjax.org/) 							展示数学公式
- [GraphViz](https://graphviz.org/) 和 [Gravizo](https://g.gravizo.com/)         显示graphviz图片
- [FontAwesome](https://fontawesome.com/)                   提供字体图标
- [Vgy.me](https://vgy.me/)                                提供图床服务



### 分支

| branch | content       |
| ------ | ------------- |
| master |               |
| dev    | markdown 文档 |
| www    | home page     |
| roots  | roots theme   |
| ace    | doc theme     |

